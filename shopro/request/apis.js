/**
 * 接口列表文件
 */
export default {

	/** 公共 ↓ **/
	common: {
		init: {
			url: "index/init",
			auth: false,
			method: "GET",
			// desc: '初始化数据',end
		},
		chat: {
			url: "chat.index/init",
			auth: false,
			method: "GET",
			// desc: '初始化聊天配置',
		},
		upload: {
			url: "index/upload",
			auth: true,
			method: "POST",
			// desc: '上传图片',
		},
		template: {
			url: "index/template",
			auth: false,
			method: "GET",
			// desc: '模板信息',end
		},
		custom: {
			url: "index/custom",
			auth: false,
			method: "GET",
			// desc: '自定义模板页面',
		},
		live: {
			url: "live",
			auth: false,
			method: "GET",
			// desc: '直播列表',
		},
		wxJssdk: {
			url: "wechat/jssdk",
			auth: false,
			method: "POST",
			// desc: '微信Jssdk',
		},
		syncPages: {
			url: "index/asyncPages",
			auth: false,
			method: "POST",
			// desc: '路由表 DEV开发模式下有效',end
		},
		debug: {
			url: "index/debugLog",
			auth: false,
			method: "POST",
			// desc: '上传DEBUG信息',
		},
		richText: {
			url: "index/richtext",
			auth: false,
			method: "GET",
			// desc: '富文本数据',
		},
		shareAdd: {
			url: "share/add",
			auth: false,
			method: "POST",
			// desc: '添加分享记录',
		},
		smsSend: {
			url: "/api/sms/send",
			auth: false,
			method: "POST",
			// desc: '发送短信',
		},
		template_index: {
			url: "/api/index/index",
			auth: false,
			method: "POST",
			// desc: '首页模板信息',end
		},
		template_service: {
			url: "/api/service/indexService",
			auth: false,
			method: "POST",
			// desc: '首页服务id',end
		},
		cate_list: {
			url: "/api/order/lists",
			auth: false,
			method: "POST",
			// desc: '首页模板信息',end
		},
		cate_order: {
			url: "/api/order/pay",
			auth: false,
			method: "POST",
			// desc: '商邦订单支付接口',end
		},
		cate_cancel: {
			url: "/api/order/cancel",
			auth: false,
			method: "POST",
			// desc: '首页模板信息',end
		},
		cate_afterSales: {
			url: "/api/order/afterSales",
			auth: false,
			method: "POST",
			// desc: '首页模板信息',end
		},
		cate_detail: {
			url: "/api/order/detail",
			auth: false,
			method: "GET",
			// desc: '首页模板信息',end
		},
		cate_comment: {
			url: "/api/order/comment",
			auth: false,
			method: "POST",
			// desc: '首页模板信息',end
		},
		cate_goods_comment: {
			url: "/api/order/commentDetail",
			auth: false,
			method: "GET",
			// desc: '首页模板信息',end
		},
		cate_goods_comment_list: {
			url: "/api/order/commentLists",
			auth: false,
			method: "GET",
			// desc: '首页模板信息',end
		},
		cate_shangquan: {
			url: "/api/store/getShangquan",
			auth: true,
			method: "GET",
			// desc: '首页模板信息',end
		},
		cate_shangchang: {
			url: "/api/store/getShangchang",
			auth: true,
			method: "GET",
			// desc: '首页模板信息',end
		},
		cate_goods_trans: {
			url: "/api/store/getTrans",
			auth: true,
			method: "GET",
			// desc: '首页模板信息',end
		},
		cate_areas: {
			url: "/api/store/getAreas",
			auth: false,
			method: "GET",
			// desc: '首页模板信息',end
		},
		huixuan_template: {
			url: "/api/huixuan/index",
			auth: false,
			method: "POST",
			// desc: '惠选模板信息',end
		},
		getmibaoquan: {
			url: "/api/storecoupon/userget",
			auth: true,
			method: "POST",
			// desc: '惠选模板信息',end
		},
		cityList: {
			url: "/api/index/cityList",
			auth: false,
			method: "POST",
			// desc: '惠选模板信息',end
		},
		isPay: {
			url: "/api/team/isPay",
			auth: false,
			method: "POST",
			// desc: '惠选模板信息',end
		},
	},

	/** 分类 ↓ **/
	category: {
		info: {
			url: "category/detail",
			auth: false,
			method: "GET",
			// desc: '分类类别',
		},
		detail: {
			url: "category",
			auth: false,
			method: "GET",
			// desc: '分类详情',
		},
		list: {
			url: "category/list",
			auth: false,
			method: "GET",
			// desc: '首页分类商品列表',
		},
		serve: {
			url: "/api/index/allShopCate",
			auth: false,
			method: "POST",
			// desc: '首页服务分类列表',
		},
		serveAllList: {
			url: "/api/service/allShopService",
			auth: false,
			method: "POST",
			// desc: '首页服务分类列表',
		},
		serveAllLists:{
			url: "/api/index/allShopCate",
			auth: false,
			method: "POST",
			// desc: '首页服务分类列表',
		},
		serveListOnes: {
			url: "/api/index/sonCate",
			auth: false,
			method: "POST",
			// desc: '首页服务分类列表',
		},
		serveList: {
			url: "/api/service/getServiceList",
			auth: false,
			method: "POST",
			// desc: '首页服务分类列表',
		},
		serveInfo: {
			url: "/api/service/getDetail",
			auth: false,
			method: "POST",
			// desc: '首页服务详情',
		},
		xianshi_list: {
			url: "/addons/shopro/goods/lists",
			auth: false,
			method: "POST",
			// desc: '秒杀商品列表',
		},
		zhekou_list: {
			url: "/addons/shopro/goods/lists",
			auth: false,
			method: "POST",
			// desc: '折扣商品列表',
		},
		yushou_list: {
			url: "/addons/shopro/goods/lists",
			auth: false,
			method: "POST",
			// desc: '预售商品列表',
		},
		shangbangTop: {
			url: "/api/index/shangbangTop",
			auth: false,
			method: "POST",
			// desc: '商帮顶部-分类-轮播图',
		},
		allShopCate: {
			url: "/api/index/allShopCate",
			auth: false,
			method: "POST",
			// desc: '全部服务',
		},
		shopLists: {
			url: "/api/index/shopLists",
			auth: false,
			method: "POST",
			// desc: '全部服务',
		},
		getCateByType: {
			url: "/api/index/getCateByType",
			auth: false,
			method: "POST",
			// desc: '全部服务',
		},
		shopDetail: {
			url: "/api/shop/detail",
			auth: false,
			method: "POST",
			// desc: '全部服务',
		},
		commentLists: {
			url: "/api/order/commentLists",
			auth: false,
			method: "POST",
			// desc: '全部服务',/api/order/commentLists

		},
		orderPre: {
			url: "/api/order/pre",
			auth: false,
			method: "POST",
			// desc: '全部服务',/api/order/commentLists
		},
		orderPay: {
			url: "/api/order/pay",
			auth: false,
			method: "POST",
			// desc: '全部服务',/api/order/commentLists
		},
		orderComment: {
			url: "/api/order/comment",
			auth: false,
			method: "POST",
			// desc: '全部服务',/api/order/commentLists
		},

		commentDetail: {
			url: "/api/order/commentDetail",
			auth: false,
			method: "POST",
			// desc: '全部服务',/api/order/commentLists
		},
		isApply: {
			url: "/api/shop/isApply",
			auth: false,
			method: "POST",
			// desc: '全部服务',/api/order/commentLists
		},
		shopInto: {
			url: "/api/shop/shopInto",
			auth: false,
			method: "POST",
			// desc: '全部服务',/api/order/commentLists
		},
		favorite: {
			url: "/api/shop/favorite",
			auth: false,
			method: "POST",
			// desc: '全部服务',/api/order/commentLists
		},
		storeCate: {
			url: "/api/store/cate",
			auth: false,
			method: "POST",
			// desc: '全部服务',/api/order/commentLists
		},
		userget: {
			url: "/api/storecoupon/userget",
			auth: false,
			method: "POST",
			// desc: '领取优惠券',
		},
		getcategoryList: {
			url: "/api/proprietary/categoryList",
			auth: false,
			method: "GET",
			// desc: '获取自营超市分类',
		},
		getcategoryDataList: {
			url: "/addons/shopro/goods/ziyingList",
			auth: false,
			method: "POST",
			// desc: '获取自营超市分类下的商品',
		}
	},


	/** 门店 ↓ **/
	store: {
		list: {
			url: "store/index",
			auth: true,
			method: "GET",
			// desc: '商户列表，不需要storeId',
		},
		info: {
			url: "store.store/index",
			auth: true,
			method: "GET",
			// desc: '商户信息',
		},
		order: {
			url: "store.order/index",
			auth: true,
			method: "GET",
			// desc: '商户订单',
		},
		orderDetail: {
			url: "store.order/detail",
			auth: true,
			method: "GET",
			// desc: '订单详情',
		},
		orderSend: {
			url: "store.order/send",
			auth: true,
			method: "POST",
			// desc: '订单发货',
		},
		orderConfirm: {
			url: "store.order/confirm",
			auth: true,
			method: "POST",
			// desc: '核销订单',
		},
		apply: {
			url: "store.apply/apply",
			auth: true,
			method: "POST",
			// desc: '申请门店',
		},
		apply_gys: {
			url: "/api/huixuan/apply",
			auth: true,
			method: "POST",
			// desc: '申请门店',
		},
		shopInfo: {
			url: "store.apply/info",
			auth: true,
			method: "POST",
			// desc: '门店信息',
		}
	},

	/** 商品 ↓ **/
	goods: {
		goodsTips:{
			url: "/api/userinfo/goodsTips",
			auth: true,
			method: "GET",
			// desc: '预售提醒',
		},
		lists: {
			url: "goods/lists",
			auth: false,
			method: "GET",
			// desc: '商品列表',
		},
		huixuanlist: {
			url: '/addons/shopro/goods/lists',
			auth: false,
			method: "POST",
			// desc: '惠选底部商品列表',
		},
		seckillList: {
			url: "/addons/shopro/goods/seckillList",
			auth: false,
			method: "POST",
			// desc: '多米秒杀列表',
		},
		zhekouList: {
			url: "/addons/shopro/goods/zhekouList",
			auth: false,
			method: "POST",
			// desc: '多米折扣列表',
		},
		rexiaoList: {
			url: "/addons/shopro/goods/rexiaoList",
			auth: false,
			method: "POST",
			// desc: '多米热销列表',
		},
		yushouList: {
			url: "/addons/shopro/goods/yushouList",
			auth: false,
			method: "POST",
			// desc: '多米秒杀列表',
		},
		activity_top: {
			url: "/api/huixuan/topgoods",
			auth: false,
			method: "POST",
			// desc: '活动顶部的商品',
		},
		huixuanclasslist: {
			url: "/addons/shopro/goods/lists",
			auth: false,
			method: "POST",
			// desc: '惠选服务分类商品列表',
		},
		activity: {
			url: "goods/activity",
			auth: false,
			method: "GET",
			// desc: '活动商品',
		},
		myGroupon: {
			url: "activity_groupon/myGroupon",
			auth: true,
			method: "GET",
			// desc: '我的拼团',
		},
		grouponDetail: {
			url: "activity_groupon/detail",
			auth: false,
			method: "GET",
			// desc: '拼团详情',
		},
		grouponItem: {
			url: "activity_groupon/index",
			auth: false,
			method: "GET",
			// desc: '拼购列表',
		},
		grouponList: {
			url: "goods/grouponList",
			auth: false,
			method: "GET",
			// desc: '拼团商品列表',
		},
		getShopDetail: {
			url: '/addons/shopro/goods/detail',
			auth: false,
			method: "POST",
			// desc: '获取多米商品详情',
		},
		getVipDetail: {
			url: '/addons/shopro/score_goods_sku_price/detail',
			auth: false,
			method: "GET",
			// desc: '获取积分商城商品详情',
		},
		detail: {
			url: "goods/detail",
			auth: false,
			method: "GET",
			// desc: '商品详情',
		},
		favorite: {
			url: "/addons/shopro/goods/favorite",
			auth: true,
			method: "POST",
			// desc: '多米商品收藏',
		},

		storeAddress: {
			url: "goods/store",
			auth: true,
			method: "GET",
			// desc: '商品支持的自提点',
		},
		scoreList: {
			url: "score_goods_sku_price/index",
			auth: false,
			method: "GET",
			// desc: '积分商品列表',
		},
		scoreDetail: {
			url: "score_goods_sku_price/detail",
			auth: false,
			method: "GET",
			// desc: '积分详情',
		},
		commentList: {
			url: "goods_comment/index",
			auth: false,
			method: "GET",
			// desc: '商品评论列表',
		},
		commentType: {
			url: "goods_comment/type",
			auth: false,
			method: "GET",
			// desc: '商品评论分类',
		}
	},

	/** 用户 ↓ **/
	user: {

		accountLogin: {
			url: "user/accountLogin",
			auth: false,
			method: "POST",
			// desc: '账号密码登录',
		},

		smsLogin: {
			url: "/api/user/mobilelogin",
			auth: false,
			method: "POST",
			// desc: '短信登录',
		},

		register: {
			url: "user/register",
			auth: false,
			method: "POST",
			// desc: '注册',
		},

		forgotPwd: {
			url: "user/forgotPwd",
			auth: false,
			method: "POST",
			// desc: '重置密码',
		},

		bindMobile: {
			url: "user/bindMobile",
			auth: true,
			method: "POST",
			// desc: '修改手机号',
		},

		changePwd: {
			url: "user/changePwd",
			auth: true,
			method: "POST",
			// desc: '修改密码',
		},

		info: {
			url: "/api/userinfo/index",
			auth: true,
			method: "POST",
			// desc: '用户信息',end
		},

		profile: {
			url: "user/profile",
			auth: true,
			method: "POST",
			// desc: '修改用户信息',end
		},
		logout: {
			url: "user/logout",
			auth: true,
			method: "POST",
			// desc: '退出登录',end
		},

		getWxMiniProgramSessionKey: {
			url: "user/getWxMiniProgramSessionKey",
			auth: false,
			method: "POST",
			// desc: '获取用户session_key',
		},
		// getWxMiniProgramtoken: {
		// 	url: "/addons/shopro/user/wxMiniProgramOauth2",
		// 	auth: false,
		// 	method: "POST",
		// 	// desc: '获取用户token',
		// },

		wxMiniProgramOauth: {
			url: "/addons/shopro/user/wxMiniProgramOauth2",
			auth: false,
			method: "POST",
			// desc: '多米微信小程序登录',
		},
		wxMiniProgramOauth_isbind: {
			url: "/addons/shopro/user/isBind",
			auth: false,
			method: "POST",
			// desc: '多米微信小程序登录是否绑定手机号',
		},
		wxMiniProgramOauth_bind: {
			url: "/addons/shopro/user/mobileBind",
			auth: false,
			method: "POST",
			// desc: '多米微信小程序登录绑定手机号',
		},
		wxOpenPlatformOauth: {
			url: "user/wxOpenPlatformOauth",
			auth: false,
			method: "POST",
			// desc: '微信APP登录',
		},

		thirdOauthInfo: {
			url: "user/thirdOauthInfo",
			auth: true,
			method: "GET",
			// desc: '第三方绑定信息',
		},
		unbindThirdOauth: {
			url: "user/unbindThirdOauth",
			auth: true,
			method: "POST",
			// desc: '解绑信息',
		},

		signList: {
			url: "user_sign/index",
			auth: true,
			method: "GET",
			// desc: '签到记录',
		},
		sign: {
			url: "user_sign/sign",
			auth: true,
			method: "POST",
			// desc: '签到',
		},
		messageIds: {
			url: "notification/template",
			auth: false,
			method: "GET",
			// desc: '订阅消息模板ids',
		},
		favoriteList: {
			url: "goods/favoriteList",
			auth: true,
			method: "GET",
			// desc: '商品收藏列表',
		},
		viewList: {
			url: "goods/viewList",
			auth: true,
			method: "GET",
			// desc: '足迹列表',end
		},
		viewDelete: {
			url: "goods/viewDelete",
			auth: true,
			method: "POST",
			// desc: '删除足迹',end
		},
		userData: {
			url: "user/userData",
			auth: true,
			method: "GET",
			// desc: '用户其他信息',
		},
		appleIdOauth: {
			url: "user/appleIdOauth",
			auth: false,
			method: "POST",
			// desc: 'appleId授权',
		},
		hongbao: {
			url: "/api/userinfo/hongbao",
			auth: false,
			method: "POST",
			// desc: '店铺详情',
		},
		haibaoget: {
			url: "/api/userinfo/haibaoget",
			auth: false,
			method: "POST",
			// desc: '海报优惠券领取',
		},
		isGegHongbao: {
			url: "/api/userinfo/isGegHongbao",
			auth: false,
			method: "POST",
			// desc: '海报优惠券领取',
		},
		isVip: {
			url: "/addons/shopro/user/getUser",
			auth: false,
			method: "get",
			// desc: '是否是会员',
		},
		getVip: {
			url: "/api/vip/getVip",
			auth: false,
			method: "get",
			// desc: '会员价格',
		}
	},

	// VIP
	vip: {
		// 充值记录
		vipOrderList: {
			url: "/api/vip/vipOrderList",
			auth: false,
			method: "POST",
		},
		// 卡密激活
		useCamilo: {
			url: "/api/vip/useCamilo",
			auth: false,
			method: "POST",
		},
		// 积分记录
		scoreList: {
			url: "/api/vip/scoreList",
			auth: false,
			method: "POST",
		},
		// 获取会员权益介绍，邀请好友送多少积分
		getXieInfo: {
			url: "/api/vip/getXieInfo",
			auth: false,
			method: "POST",
		},
		// vip价格列表
		vipList: {
			url: "/api/vip/vipList",
			auth: false,
			method: "POST",
		},
		// 购买vip
		createVipOrder: {
			url: "/api/vip/createVipOrder",
			auth: false,
			method: "POST",
		},
		// 团购参与人员列表
		pinkInfo: {
			url: "/api/vip/pinkInfo",
			auth: true,
			method: "POST",
		},
		// 团购参与人员列表
		pinkInfo_go: {
			url: "/api/team/timeRev",
			auth: false,
			method: "POST",
		},
		//查看助力人员列表
		helpInfo: {
			url: "/api/vip/helpInfo",
			auth: true,
			method: "POST",
		},
		//切换砍价列表
		tabHelp: {
			url: "/api/vip/tabHelp",
			auth: true,
			method: "POST",
		},
		//删除团购订单
		delPink: {
			url: "/api/vip/delPink",
			auth: false,
			method: "POST",
		},
		// 删除砍价订单
		delHelp: {
			url: "/api/vip/delHelp",
			auth: false,
			method: "POST",
		},
		//创建助力订单
		createHelpOrder: {
			url: "/api/vip/createHelpOrder",
			auth: false,
			method: "POST",
		},

		//帮忙助力
		helpFriend: {
			url: "/api/vip/helpFriend",
			auth: false,
			method: "POST",
		},
		//我的页面修改个人信息
		profile: {
			url: "/api/user/profile",
			auth: false,
			method: "POST",
		},
		updateOrder: {
			url: "/api/vip/updateOrder",
			auth: true,
			method: "POST",
			// desc: '倒计时结束',
		},
		// 根据订单获取用户头像
		orderAvatar: {
			url: "/api/vip/orderAvatar",
			auth: false,
			method: "POST",
		},
		pinklists: {
			url: "/api/team/lists",
			auth: true,
			method: "POST",
			// desc: '拼团列表',
		},
		helplists: {
			url: "/api/team/helpLists",
			auth: true,
			method: "POST",
			// desc: '砍价详情',
		},
		pinklists_detail: {
			url: "/api/team/detail",
			auth: false,
			method: "POST",
			// desc: '拼团详情',
		},
		helplists_detail: {
			url: "/api/team/helpDetail",
			auth: true,
			method: "POST",
			// desc: '砍价详情',
		},
	},

	/** 位置 ↓ **/
	address: {
		area: {
			url: "address/area",
			auth: false,
			method: "GET",
			// desc: '省市区',
		},
		list: {
			url: "address",
			auth: true,
			method: "GET",
			// desc: '地址列表',
		},
		edit: {
			url: "address/edit",
			auth: true,
			method: "POST",
			// desc: '修改地址',
		},
		defaults: {
			url: "/addons/shopro/address/defaults",
			auth: true,
			method: "GET",
			// desc: '多米商城默认地址',
		},
		info: {
			url: "address/info",
			auth: true,
			method: "GET",
			// desc: '地址详情',
		},
		del: {
			url: "address/del",
			auth: true,
			method: "POST",
			// desc: '删除',
		},
	},

	/** 常见问题 ↓ **/
	other: {
		faqList: {
			url: "faq",
			auth: false,
			method: "GET",
			// desc: '常见问题列表',
		},
		feedbackType: {
			url: "feedback/type",
			auth: true,
			method: "GET",
			// desc: '意见反馈类型',
		},
		feedbackAdd: {
			url: "feedback/add",
			auth: true,
			method: "POST",
			// desc: '提交意见',
		},
		commentAdd: {
			url: "comment/submit",
			auth: true,
			method: "POST",
			// desc: '提交评论',
		},
		commentList: {
			url: "comment/list",
			auth: true,
			method: "GET",
			// desc: '评论列表',
		}
	},

	/** 购物车 ↓ **/
	cart: {
		choosedgood: {
			url: "/addons/shopro/cart/setSelect",
			auth: true,
			method: "POST",
			// desc: '多米购物车选中商品列表',
		},
		indexs: {
			url: "/addons/shopro/cart",
			auth: true,
			method: "POST",
			// desc: '多米购物车商品列表',
		},
		adds: {
			url: "/addons/shopro/cart/add",
			auth: true,
			method: "POST",
			// desc: '多米添加购物车',
		},
		edits: {
			url: "/addons/shopro/cart/edit",
			auth: true,
			method: "POST",
			// desc: '多米编辑购物车',
		},
	},
	// 商邦订单
	cate: {
		list: {
			url: "api/order/lists",
			auth: true,
			method: "POST",
			// desc: '提交订单运单号',
		},
	},
	/** 订单 ↓ **/
	order: {
		order_yundanhao: {
			url: "/addons/shopro/order_aftersale/saveReturnExpress",
			auth: true,
			method: "POST",
			// desc: '提交订单运单号',
		},
		index: {
			url: "order/index",
			auth: true,
			method: "GET",
			// desc: '订单列表',
		},
		pre: {
			url: "/addons/shopro/order/pre",
			auth: true,
			method: "POST",
			// desc: '多米商城预备提交订单',
		},
		createOrder: {
			url: "/addons/shopro/order/createOrder",
			auth: true,
			method: "POST",
			// desc: '多米商城提交订单',
		},
		detail: {
			url: "order/detail",
			auth: true,
			method: "GET",
			// desc: '订单详情',
		},
		itemDetail: {
			url: "order/itemDetail",
			auth: true,
			method: "GET",
			// desc: '订单商品详情',
		},
		confirm: {
			url: "order/confirm",
			auth: true,
			method: "POST",
			// desc: '确认收货',
		},
		cancel: {
			url: "order/cancel",
			auth: true,
			method: "POST",
			// desc: '取消订单',
		},
		comment: {
			url: "order/comment",
			auth: true,
			method: "POST",
			// desc: '评价商品',
		},
		coupons: {
			url: "order/coupons",
			auth: true,
			method: "POST",
			// desc: '商品可用优惠券',
		},
		aftersale: {
			url: "order_aftersale/aftersale",
			auth: true,
			method: "POST",
			// desc: '申请售后',
		},
		aftersaleList: {
			url: "order_aftersale/index",
			auth: true,
			method: "GET",
			// desc: '售后列表',
		},
		aftersaleDetail: {
			url: "order_aftersale/detail",
			auth: true,
			method: "GET",
			// desc: '售后列表详情',
		},
		deleteOrder: {
			url: "order/delete",
			auth: true,
			method: "POST",
			// desc: '删除订单',
		},
		deleteAftersaleOrder: {
			url: "order_aftersale/delete",
			auth: true,
			method: "POST",
			// desc: '删除售后订单',
		},
		cancelAftersaleOrder: {
			url: "order_aftersale/cancel",
			auth: true,
			method: "POST",
			// desc: '取消售后订单',
		},
		expressList: {
			url: "order_express/index",
			auth: true,
			method: "GET",
			// desc: '包裹列表',
		},
		expressDetail: {
			url: "order_express/detail",
			auth: true,
			method: "GET",
			// desc: '包裹详情',
		}
	},

	/** 支付金钱 ↓ **/
	money: {
		prepay: {
			url: "pay/prepay",
			auth: true,
			method: "POST",
			// desc: '发起支付',
		},
		prepays: {
			url: "/addons/shopro/pay/prepay",
			auth: true,
			method: "POST",
			// desc: '购物金发起支付',
		},
		walletApply: {
			url: "user_wallet_apply/apply",
			auth: true,
			method: "POST",
			// desc: '申请提现',end
		},
		walletRule: {
			url: "user_wallet_apply/rule",
			auth: true,
			method: "GET",
			// desc: '提现规则',end
		},
		walletLog: {
			url: "user_wallet_log",
			auth: true,
			method: "GET",
			// desc: '钱包,积分明细',end
		},
		bankInfo: {
			url: "user_bank/info",
			auth: true,
			method: "GET",
			// desc: '银行卡信息',end
		},
		bankEdit: {
			url: "user_bank/edit",
			auth: true,
			method: "POST",
			// desc: '编辑银行卡信息',end
		},
		withdrawLog: {
			url: "user_wallet_apply/index",
			auth: true,
			method: "GET",
			// desc: '提现记录',end
		}
	},

	/** 优惠券 ↓ **/
	coupons: {
		list: {
			url: "coupons",
			auth: false,
			method: "GET",
			// desc: '个人中心优惠券列表',
		},
		templateList: {
			url: "coupons/lists",
			auth: false,
			method: "GET",
			// desc: '首页优惠券',
		},
		//多米
		get: {
			url: "/api/userinfo/isGetYouhui",
			auth: true,
			method: "POST",
			// desc: '判断是否领取过',
		},
		getyouhuiquan: {
			url: "/api/userinfo/newUser",
			auth: true,
			method: "POST",
			// desc: '领取',
		},
		detail: {
			url: "coupons/detail",
			auth: true,
			method: "GET",
			// desc: '购物券详情',
		},
		goods: {
			url: "coupons/goods",
			auth: false,
			method: "GET",
			// desc: '适用商品',
		}
	},

	/** 分销 ↓ **/
	commission: {
		auth: {
			url: "commission.agent/info",
			auth: true,
			method: "GET",
			// desc: '分销身份鉴权',
		},
		log: {
			url: "commission.log/index",
			auth: true,
			method: "GET",
			// desc: '分销动态',
		},
		goods: {
			url: "commission.goods/index",
			auth: true,
			method: "GET",
			// desc: '分销商品',
		},
		ranking: {
			url: "commission.agent/ranking",
			auth: true,
			method: "GET",
			// desc: '分销商排行榜',
		},
		form: {
			url: "commission.agent/agentForm",
			auth: true,
			method: "GET",
			// desc: '申请成为分销商表单',
		},
		apply: {
			url: "commission.agent/applyForm",
			auth: true,
			method: "POST",
			// desc: '申请成为分销商',
		},
		team: {
			url: "commission.agent/team",
			auth: true,
			method: "GET",
			// desc: '我的团队',
		},
		share: {
			url: "share/index",
			auth: true,
			method: "GET",
			// desc: '分享记录',
		},
		orderLog: {
			url: "commission.order/index",
			auth: true,
			method: "GET",
			// desc: '分销订单',
		},
		rewardLog: {
			url: "commission.reward/index",
			auth: true,
			method: "GET",
			// desc: '佣金记录',
		},
		lv: {
			url: "commission.agent/level",
			auth: true,
			method: "GET",
			// desc: '佣金记录',
		}
	},
	/** 我的 ↓ **/
	mine: {
		favlists: {
			url: "/api/shop/favlists",
			auth: true,
			method: "POST",
			// desc: '收藏-多米商邦',
		},
		favoriteList: {
			url: "/addons/shopro/goods/favoriteList",
			auth: true,
			method: "POST",
			// desc: '收藏-多米惠选',
		},
		viewLists: {
			url: "/api/shop/viewLists",
			auth: true,
			method: "POST",
			// desc: '足迹-多米商邦',
		},
		viewListsDel: {
			url: "/api/shop/viewListsDelGoods",
			auth: true,
			method: "POST",
			// desc: '足迹-多米商邦--删除商品',
		},

		viewList: {
			url: "/addons/shopro/goods/viewList",
			auth: true,
			method: "POST",
			// desc: '足迹-多米惠选',
		},

		viewListDel: {
			url: "/addons/shopro/goods/viewDelete",
			auth: true,
			method: "POST",
			// desc: '足迹-多米惠选--删除商品',
		},

		invoiceList: {
			url: "/api/invoice/index",
			auth: true,
			method: "POST",
			// desc: '发票管理列表',
		},
		openInvoice: {
			url: "/api/invoice/openInvoice",
			auth: true,
			method: "POST",
			// desc: '提交发票信息',
		},
		myQrcode: {
			url: "/api/invoice/myQrcode",
			auth: true,
			method: "POST",
			// desc: '我的二维码',
		},
		getAdviceType: {
			url: "/api/invoice/adviceType",
			auth: true,
			method: "POST",
			// desc: '问题类型',
		},
		addTakeAdvice: {
			url: "/api/invoice/takeAdvice",
			auth: true,
			method: "POST",
			// desc: '提交意见反馈',
		},
		getPayPwdStatus: {
			url: "/api/user/payPwdStatus",
			auth: true,
			method: "POST",
			// desc: '获取支付密码设置状态',
		},
		setPayPwdStatus: {
			url: "/api/user/setPayPwd",
			auth: true,
			method: "POST",
			// desc: '设置或新增支付密码',
		},
		orderList: {
			url: "/api/order/lists",
			auth: true,
			method: "POST",
			// desc: '消费明细列表',
		},
		isPaySetPwd: {
			url: "/api/userinfo/isPaySetPwd",
			auth: true,
			method: "POST",
			// desc: '检查是否设置支付密码',
		},
		isPayPwdTrue: {
			url: "/api/userinfo/checkPayPwd",
			auth: true,
			method: "POST",
			// desc: '检查是否设置支付密码',
		}
	},
	my: {
		gouwujin: {
			url: "/api/userinfo/gouwujin",
			auth: true,
			method: "POST",
		},
		daijinquan: {
			url: "/api/userinfo/daijinquan",
			auth: true,
			method: "POST",
		},
		youhuiquan: {
			url: "/api/userinfo/youhuiquan",
			auth: true,
			method: "POST",
		},
		mibao: {
			url: "/api/mibao/lists",
			auth: true,
			method: "POST",
		},
		takeRealInfo: {
			url: "/api/user/takeRealInfo",
			auth: true,
			method: "POST",
		},
		msgList: {
			url: "/api/invoice/msgList",
			auth: true,
			method: "POST",
		},
		profile: {
			url: "/api/user/profile",
			auth: true,
			method: "POST",
		},
		chargeMoney: {
			url: "/api/income/rechargeGouwu",
			auth: true,
			method: "POST",
		},
		// chargeGouwujin:{
		// 	url: "/api/income/rechargeGouwu",
		// 	auth: true,
		// 	method: "POST",
		// },
		getGiveUserInfo: {
			url: "/api/income/getUser",
			auth: true,
			method: "POST",
		},
		giveDaijinquan: {
			url: "/api/income/give",
			auth: true,
			method: "POST",
		},
	},
	// 服务
	service: {
		serviceList: {
			url: "/api/userinfo/gouwujin",
			auth: true,
			method: "POST",
		},
	}


};
