// 路由
import {
	RouterMount,
	createRouter
} from './uni-simple-router.js'
import store from '@/shopro/store'
const router = createRouter({
	platform: process.env.VUE_APP_PLATFORM,
	applet: {
		animationDuration: 0 //默认 300ms
	},
	routerErrorEach: ({
		type,
		msg
	}) => {
		switch (type) {
			case 3: // APP退出应用
				// #ifdef APP-PLUS
				router.$lockStatus = false;
				uni.showModal({
					title: '提示',
					content: '您确定要退出应用吗？',
					success: function(res) {
						if (res.confirm) {
							plus.runtime.quit();
						}
					}
				});
				// #endif
				break;
			case 2:
			case 0:
				router.$lockStatus = false;
				break;
			default:
				break;
		}

	},
	// 通配符，非定义页面，跳转404
	routes: [...ROUTES,
		{
			path: '*',
			redirect: (to) => {
				return {
					name: '404'
				}
			}
		},
	]
});
console.log("==============router",router)
//全局路由前置守卫
router.beforeEach((to, from, next) => {
   console.log("========!from",from.query.scene,from)
   if(from.query.scene){
    let arr =  from.query.scene.split("&");
      let arr1 =  arr[0].split("=")[1];
      let storeShopId = arr1;
      store.dispatch('storeShopId',storeShopId);
   }
   if(from.fullPath=='/pages/index/user'){
     uni.setStorageSync('user_pages', true)
   }else{
     uni.setStorageSync('user_pages', false)
   }
   if (from.query.typeTitle) {
       if(from.query.typeTitle=="1"){
        uni.setStorageSync('vip_pink_id', from.query.id)
        uni.setStorageSync('typeTitle', from.query.typeTitle)
       }else{
        uni.setStorageSync('typeTitle', from.query.typeTitle)
       }
   }
   if (from.query.screnVipShareObj) uni.setStorageSync('screnVipShareObj', from.query.screnVipShareObj)
   if (from.query.userBindId) uni.setStorageSync('user_bind_id', from.query.userBindId)
	// 权限控制登录
    if (to.meta && to.meta.auth && !store.getters.isLogin) {
        console.log("========!store.getters.isLogin",store.getters.isLogin,!store.getters.isLogin,"======to.meta",to.meta,"==== to.meta.auth",to.meta.auth,"========!store.getters.isLogin",!store.getters.isLogin)
         next({
          path: '/pages/index/login',                //无登录状态,先跳转到登录页面
          query: { redirected: to.fullPath } // 登录成功后跳转回到该路由
        })
        store.dispatch('showAuthModal','toLogins');
    } else {
        next()
    }
});


export {
	router,
	RouterMount
}
